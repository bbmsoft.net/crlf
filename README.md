# CRLF

A command line utility for converting between windows and unix style line endings, implemented in Rust.

The project contains both a library providing some basic conversion functionality as well as two executables, namely ```crlf``` and ```lf```, that convert text either from a file or from stdin into windows and unix style line endings respectively.

The motivation behind this project was mainly to gain some more practical experience with the Rust programming language and to get a useful tool on the way.

## How to install

If you have a Rust tool chain set up on your machine, run:

```
git clone https://gitlab.com/bbmsoft.net/crlf.git
cd crlf
cargo install
```

This will compile the executables and put them into your cargo bin dir, which is most probably already in your PATH.

This should work on every platform Rust is available for, however I am currently testing mostly on Linux and occasionally on Windows.

For those who do not have a Rust tool chain set up on their machine, I may provide compiled executables at some time in the future. But please don't hold your breath for it, it's not actually on my TODO list right now.

## How to use

Both ```crlf``` and ```lf``` have the same syntax. Here are some examples:

```crlf path/to/FILE > path/to/NEWFILE``` converts FILE to windows style line endings and saves the output to NEWFILE.

```grep "foo" path/to/FILE | lf > path/to/NEWFILE``` gets all lines containing 'foo' from FILE and writes them to NEWFILE with unix style line endings.

## Caveats

Currently CRLF is being tested only with UTF-8, ASCII and CP1252 encoded input. Text in other encodings may break when being processes by CRLF.

CRLF should work as expected on Linux and Windows machines, however tests on Windows are sporadic and may overlook things. Other platforms are not being tested at the moment, however I would expect CRLF to produce the same results on any platform Rust can compile to.