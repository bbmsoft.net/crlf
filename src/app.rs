use clap::App;
use std::io::{stdout, Result, Write};

type LineProcessor = fn(Vec<u8>) -> Result<()>;
type Finalizer = fn() -> Result<()>;
type FileConverter = fn(&str) -> Result<Vec<Vec<u8>>>;
type LineByLineFileConverter = fn(&str, LineProcessor) -> Result<()>;
type StdInConverter = fn() -> Result<Vec<Vec<u8>>>;
type LineByLineStdInConverter = fn(LineProcessor) -> Result<()>;

pub fn run(
    app: App,
    file_converter: FileConverter,
    line_by_line_file_converter: LineByLineFileConverter,
    stdin_converter: StdInConverter,
    line_by_line_stdin_converter: LineByLineStdInConverter,
) {
    let matches = app.get_matches();

    if matches.is_present("parallel") {
        match matches.value_of("FILE") {
            Some(file) => convert_file(file, file_converter),
            None => convert_stdin(stdin_converter),
        }
    } else {
        match matches.value_of("FILE") {
            Some(file) => convert_file_line_by_line(file, line_by_line_file_converter),
            None => convert_stdin_line_by_line(line_by_line_stdin_converter),
        }
    }
}

fn convert_file(file: &str, file_converter: FileConverter) {
    if let Err(err) = print(file_converter(file)) {
        eprintln!("Failed to convert file '{}': {}", file, err);
    }
}

fn convert_stdin(stdin_converter: StdInConverter) {
    if let Err(err) = print(stdin_converter()) {
        eprintln!("Failed to convert input: {}", err);
    }
}

fn print(result: Result<Vec<Vec<u8>>>) -> Result<()> {
    let flattened: Vec<u8> = result?.iter().flatten().cloned().collect();
    stdout().write_all(&flattened)
}

fn convert_file_line_by_line(file: &str, file_converter: LineByLineFileConverter) {
    if let Err(err) = convert_and_finalize(|| file_converter(file, print_line), flush) {
        eprintln!("Failed to convert file '{}': {}", file, err);
    }
}

fn convert_stdin_line_by_line(stdin_converter: LineByLineStdInConverter) {
    if let Err(err) = convert_and_finalize(|| stdin_converter(print_line), flush) {
        eprintln!("Failed to convert input: {}", err);
    }
}

fn convert_and_finalize(convert: impl Fn() -> Result<()>, finalize: Finalizer) -> Result<()> {
    convert()?;
    finalize()
}

fn print_line(line: Vec<u8>) -> Result<()> {
    stdout().write_all(&line)
}

fn flush() -> Result<()> {
    stdout().flush()
}
