#[macro_use]
extern crate clap;
extern crate crlf;

mod app;

use app::run;
use crlf::{file_all_to_crlf, file_to_crlf, stdin_all_to_crlf, stdin_to_crlf};

fn main() {
    let app = clap_app!(lf =>
        (version: env!("CARGO_PKG_VERSION"))
        (author: env!("CARGO_PKG_AUTHORS"))
        (about: "Convert text from stdin or a file to windows style line endings (CRLF).")
        (@arg parallel: -b --buffered "Buffer the entire input before processing")
        (@arg FILE: +takes_value "File to be converted")
    );

    run(
        app,
        file_all_to_crlf,
        file_to_crlf,
        stdin_all_to_crlf,
        stdin_to_crlf,
    );
}
