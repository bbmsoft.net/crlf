mod test;

use std::fs::OpenOptions;
use std::io::{stdin, BufRead, BufReader, Result, StdinLock};

pub fn file_to_crlf(path: &str, process_line: impl Fn(Vec<u8>) -> Result<()>) -> Result<()> {
    convert_from_file(path, process_line, true)
}

pub fn file_to_lf(path: &str, process_line: impl Fn(Vec<u8>) -> Result<()>) -> Result<()> {
    convert_from_file(path, process_line, false)
}

pub fn reader_to_crlf(
    reader: &mut impl BufRead,
    process_line: impl Fn(Vec<u8>) -> Result<()>,
) -> Result<()> {
    convert_from_reader(Some(reader), process_line, true)
}

pub fn reader_to_lf(
    reader: &mut impl BufRead,
    process_line: impl Fn(Vec<u8>) -> Result<()>,
) -> Result<()> {
    convert_from_reader(Some(reader), process_line, false)
}

pub fn file_all_to_crlf(file: &str) -> Result<Vec<Vec<u8>>> {
    convert_all_from_file(file, true)
}

pub fn file_all_to_lf(file: &str) -> Result<Vec<Vec<u8>>> {
    convert_all_from_file(file, false)
}

pub fn reader_all_to_crlf(reader: &mut impl BufRead) -> Result<Vec<Vec<u8>>> {
    convert_all_from_reader(Some(reader), true)
}

pub fn reader_all_to_lf(reader: &mut impl BufRead) -> Result<Vec<Vec<u8>>> {
    convert_all_from_reader(Some(reader), false)
}

pub fn stdin_to_crlf(process_line: impl Fn(Vec<u8>) -> Result<()>) -> Result<()> {
    convert_from_reader(None::<&mut StdinLock>, process_line, true)
}

pub fn stdin_to_lf(process_line: impl Fn(Vec<u8>) -> Result<()>) -> Result<()> {
    convert_from_reader(None::<&mut StdinLock>, process_line, false)
}

pub fn stdin_all_to_crlf() -> Result<Vec<Vec<u8>>> {
    convert_all_from_reader(None::<&mut StdinLock>, true)
}

pub fn stdin_all_to_lf() -> Result<Vec<Vec<u8>>> {
    convert_all_from_reader(None::<&mut StdinLock>, false)
}

pub fn par_file_to_crlf(path: &str, process_line: impl Fn(Vec<u8>) -> Result<()>) -> Result<()> {
    convert_from_file(path, process_line, true)
}

fn convert_all_from_file(path: &str, to_crlf: bool) -> Result<Vec<Vec<u8>>> {
    let file = OpenOptions::new().read(true).open(path)?;

    let mut reader = BufReader::new(&file);

    convert_all_from_reader(Some(&mut reader), to_crlf)
}

fn convert_from_file(
    path: &str,
    process_line: impl Fn(Vec<u8>) -> Result<()>,
    to_crlf: bool,
) -> Result<()> {
    let file = OpenOptions::new().read(true).open(path)?;

    let mut reader = BufReader::new(&file);

    convert_from_reader(Some(&mut reader), process_line, to_crlf)
}

fn convert_all_from_reader(
    mut reader: Option<&mut impl BufRead>,
    to_crlf: bool,
) -> Result<Vec<Vec<u8>>> {
    let mut lines = Vec::new();

    while let Some(line) = match &mut reader {
        Some(reader) => read_line(reader),
        None => {
            let stdin = stdin();
            let mut lock = stdin.lock();
            read_line(&mut lock)
        }
    }? {
        lines.push(line);
    }

    for mut line in &mut lines {
        convert_line(&mut line, to_crlf);
    }

    Ok(lines)
}

fn convert_from_reader(
    mut reader: Option<&mut impl BufRead>,
    process_line: impl Fn(Vec<u8>) -> Result<()>,
    to_crlf: bool,
) -> Result<()> {
    while let Some(line) = convert_next_line(&mut reader, to_crlf)? {
        process_line(line)?;
    }

    Ok(())
}

fn convert_next_line(
    reader: &mut Option<&mut impl BufRead>,
    to_crlf: bool,
) -> Result<Option<Vec<u8>>> {
    let line = match reader {
        Some(reader) => read_line(reader),
        None => {
            let stdin = stdin();
            let mut lock = stdin.lock();
            read_line(&mut lock)
        }
    }?;

    match line {
        Some(mut line) => {
            convert_line(&mut line, to_crlf);
            Ok(Some(line))
        }
        None => Ok(None),
    }
}

fn read_line(read: &mut impl BufRead) -> Result<Option<Vec<u8>>> {
    let mut line = Vec::new();

    match read.read_until(b'\n', &mut line)? {
        0 => Ok(None),
        _ => Ok(Some(line)),
    }
}

fn convert_line(line: &mut Vec<u8>, to_crlf: bool) {
    if to_crlf {
        convert_line_to_crlf(line);
    } else {
        convert_line_to_lf(line);
    }
}

fn convert_line_to_crlf(line: &mut Vec<u8>) {
    let length = line.len();

    if length == 1 && line[0] == b'\n' {
        line.insert(0, b'\r');
    } else if line[length - 1] == b'\n' && line[length - 2] != b'\r' {
        line.insert(length - 1, b'\r');
    }
}

fn convert_line_to_lf(line: &mut Vec<u8>) {
    let length = line.len();

    if length > 1 {
        let cr_index = length - 2;
        if line[cr_index] == b'\r' {
            line.remove(cr_index);
        }
    }
}
