#[cfg(test)]
use super::{
    convert_line_to_crlf, convert_line_to_lf, convert_next_line, file_all_to_crlf, file_all_to_lf,
    reader_all_to_crlf, reader_all_to_lf, BufReader,
};

#[test]
fn test_file_all_to_lf() {
    let converted = file_all_to_lf("test-crlf.txt").unwrap();
    assert_eq!(converted[0], b"Hello, world!\n");
    assert_eq!(converted[1], b"\n");
    assert_eq!(converted[2], b"Lorem ipsum dolor sit amet\n");

    let converted = file_all_to_lf("test-lf.txt").unwrap();
    assert_eq!(converted[0], b"Hello, world!\n");
    assert_eq!(converted[1], b"\n");
    assert_eq!(converted[2], b"Lorem ipsum dolor sit amet");
}

#[test]
fn test_file_all_to_crlf() {
    let converted = file_all_to_crlf("test-crlf.txt").unwrap();
    assert_eq!(converted[0], b"Hello, world!\r\n");
    assert_eq!(converted[1], b"\r\n");
    assert_eq!(converted[2], b"Lorem ipsum dolor sit amet\r\n");

    let converted = file_all_to_crlf("test-lf.txt").unwrap();
    assert_eq!(converted[0], b"Hello, world!\r\n");
    assert_eq!(converted[1], b"\r\n");
    assert_eq!(converted[2], b"Lorem ipsum dolor sit amet");
}

#[test]
fn test_reader_all_to_lf() {
    {
        let bytes: &[u8] = b"";
        let mut reader = BufReader::new(bytes);
        assert_eq!(
            reader_all_to_lf(&mut reader).unwrap(),
            Vec::<Vec<u8>>::new()
        );
    }

    {
        let bytes: &[u8] = b"check\none\ntwo\n";
        let mut reader = BufReader::new(bytes);
        assert_eq!(
            reader_all_to_lf(&mut reader).unwrap(),
            vec!["check\n".as_bytes(), "one\n".as_bytes(), "two\n".as_bytes()]
        );
    }

    {
        let bytes: &[u8] = b"check\r\none\r\ntwo\r\n";
        let mut reader = BufReader::new(bytes);
        assert_eq!(
            reader_all_to_lf(&mut reader).unwrap(),
            vec!["check\n".as_bytes(), "one\n".as_bytes(), "two\n".as_bytes()]
        );
    }

    {
        let bytes: &[u8] = b"check\r\none\r\ntwo";
        let mut reader = BufReader::new(bytes);
        assert_eq!(
            reader_all_to_lf(&mut reader).unwrap(),
            vec!["check\n".as_bytes(), "one\n".as_bytes(), "two".as_bytes()]
        );
    }

    {
        let bytes: &[u8] = b"\r\ncheck\none\r\ntwo\n";
        let mut reader = BufReader::new(bytes);
        assert_eq!(
            reader_all_to_lf(&mut reader).unwrap(),
            vec![
                "\n".as_bytes(),
                "check\n".as_bytes(),
                "one\n".as_bytes(),
                "two\n".as_bytes()
            ]
        );
    }
}

#[test]
fn test_reader_all_to_crlf() {
    {
        let bytes: &[u8] = b"";
        let mut reader = BufReader::new(bytes);
        assert_eq!(
            reader_all_to_crlf(&mut reader).unwrap(),
            Vec::<Vec<u8>>::new()
        );
    }

    {
        let bytes: &[u8] = b"check\none\ntwo\n";
        let mut reader = BufReader::new(bytes);
        assert_eq!(
            reader_all_to_crlf(&mut reader).unwrap(),
            vec![
                "check\r\n".as_bytes(),
                "one\r\n".as_bytes(),
                "two\r\n".as_bytes()
            ]
        );
    }

    {
        let bytes: &[u8] = b"check\r\none\r\ntwo\r\n";
        let mut reader = BufReader::new(bytes);
        assert_eq!(
            reader_all_to_crlf(&mut reader).unwrap(),
            vec![
                "check\r\n".as_bytes(),
                "one\r\n".as_bytes(),
                "two\r\n".as_bytes()
            ]
        );
    }

    {
        let bytes: &[u8] = b"check\r\none\r\ntwo";
        let mut reader = BufReader::new(bytes);
        assert_eq!(
            reader_all_to_crlf(&mut reader).unwrap(),
            vec![
                "check\r\n".as_bytes(),
                "one\r\n".as_bytes(),
                "two".as_bytes()
            ]
        );
    }

    {
        let bytes: &[u8] = b"\ncheck\r\none\ntwo\r\n";
        let mut reader = BufReader::new(bytes);
        assert_eq!(
            reader_all_to_crlf(&mut reader).unwrap(),
            vec![
                "\r\n".as_bytes(),
                "check\r\n".as_bytes(),
                "one\r\n".as_bytes(),
                "two\r\n".as_bytes()
            ]
        );
    }
}
#[test]
fn test_convert_next_line_to_lf() {
    {
        let bytes: &[u8] = b"";
        let mut reader = BufReader::new(bytes);
        let mut reader = Some(&mut reader);
        assert_eq!(None, convert_next_line(&mut reader, false).unwrap());
    }

    {
        let bytes: &[u8] = b"check\none\ntwo\n";
        let mut reader = BufReader::new(bytes);
        let mut reader = Some(&mut reader);
        assert_eq!(
            "check\n",
            &String::from_utf8(convert_next_line(&mut reader, false).unwrap().unwrap()).unwrap()
        );
        assert_eq!(
            "one\n",
            &String::from_utf8(convert_next_line(&mut reader, false).unwrap().unwrap()).unwrap()
        );
        assert_eq!(
            "two\n",
            &String::from_utf8(convert_next_line(&mut reader, false).unwrap().unwrap()).unwrap()
        );
        assert_eq!(None, convert_next_line(&mut reader, false).unwrap());
    }

    {
        let bytes: &[u8] = b"check\r\none\r\ntwo\r\n";
        let mut reader = BufReader::new(bytes);
        let mut reader = Some(&mut reader);
        assert_eq!(
            "check\n",
            &String::from_utf8(convert_next_line(&mut reader, false).unwrap().unwrap()).unwrap()
        );
        assert_eq!(
            "one\n",
            &String::from_utf8(convert_next_line(&mut reader, false).unwrap().unwrap()).unwrap()
        );
        assert_eq!(
            "two\n",
            &String::from_utf8(convert_next_line(&mut reader, false).unwrap().unwrap()).unwrap()
        );
        assert_eq!(None, convert_next_line(&mut reader, false).unwrap());
    }

    {
        let bytes: &[u8] = b"check\r\none\r\ntwo";
        let mut reader = BufReader::new(bytes);
        let mut reader = Some(&mut reader);
        assert_eq!(
            "check\n",
            &String::from_utf8(convert_next_line(&mut reader, false).unwrap().unwrap()).unwrap()
        );
        assert_eq!(
            "one\n",
            &String::from_utf8(convert_next_line(&mut reader, false).unwrap().unwrap()).unwrap()
        );
        assert_eq!(
            "two",
            &String::from_utf8(convert_next_line(&mut reader, false).unwrap().unwrap()).unwrap()
        );
    }

    {
        let bytes: &[u8] = b"\r\ncheck\none\r\ntwo\n";
        let mut reader = BufReader::new(bytes);
        let mut reader = Some(&mut reader);
        assert_eq!(
            "\n",
            &String::from_utf8(convert_next_line(&mut reader, false).unwrap().unwrap()).unwrap()
        );
        assert_eq!(
            "check\n",
            &String::from_utf8(convert_next_line(&mut reader, false).unwrap().unwrap()).unwrap()
        );
        assert_eq!(
            "one\n",
            &String::from_utf8(convert_next_line(&mut reader, false).unwrap().unwrap()).unwrap()
        );
        assert_eq!(
            "two\n",
            &String::from_utf8(convert_next_line(&mut reader, false).unwrap().unwrap()).unwrap()
        );
        assert_eq!(None, convert_next_line(&mut reader, false).unwrap());
    }
}

#[test]
fn test_convert_next_line_to_crlf() {
    {
        let bytes: &[u8] = b"";
        let mut reader = BufReader::new(bytes);
        let mut reader = Some(&mut reader);
        assert_eq!(None, convert_next_line(&mut reader, true).unwrap());
    }

    {
        let bytes: &[u8] = b"check\none\ntwo\n";
        let mut reader = BufReader::new(bytes);
        let mut reader = Some(&mut reader);
        assert_eq!(
            "check\r\n",
            &String::from_utf8(convert_next_line(&mut reader, true).unwrap().unwrap()).unwrap()
        );
        assert_eq!(
            "one\r\n",
            &String::from_utf8(convert_next_line(&mut reader, true).unwrap().unwrap()).unwrap()
        );
        assert_eq!(
            "two\r\n",
            &String::from_utf8(convert_next_line(&mut reader, true).unwrap().unwrap()).unwrap()
        );
        assert_eq!(None, convert_next_line(&mut reader, true).unwrap());
    }

    {
        let bytes: &[u8] = b"check\r\none\r\ntwo\r\n";
        let mut reader = BufReader::new(bytes);
        let mut reader = Some(&mut reader);
        assert_eq!(
            "check\r\n",
            &String::from_utf8(convert_next_line(&mut reader, true).unwrap().unwrap()).unwrap()
        );
        assert_eq!(
            "one\r\n",
            &String::from_utf8(convert_next_line(&mut reader, true).unwrap().unwrap()).unwrap()
        );
        assert_eq!(
            "two\r\n",
            &String::from_utf8(convert_next_line(&mut reader, true).unwrap().unwrap()).unwrap()
        );
        assert_eq!(None, convert_next_line(&mut reader, true).unwrap());
    }

    {
        let bytes: &[u8] = b"check\none\ntwo";
        let mut reader = BufReader::new(bytes);
        let mut reader = Some(&mut reader);
        assert_eq!(
            "check\r\n",
            &String::from_utf8(convert_next_line(&mut reader, true).unwrap().unwrap()).unwrap()
        );
        assert_eq!(
            "one\r\n",
            &String::from_utf8(convert_next_line(&mut reader, true).unwrap().unwrap()).unwrap()
        );
        assert_eq!(
            "two",
            &String::from_utf8(convert_next_line(&mut reader, true).unwrap().unwrap()).unwrap()
        );
    }

    {
        let bytes: &[u8] = b"\ncheck\r\none\ntwo\r\n";
        let mut reader = BufReader::new(bytes);
        let mut reader = Some(&mut reader);
        assert_eq!(
            "\r\n",
            &String::from_utf8(convert_next_line(&mut reader, true).unwrap().unwrap()).unwrap()
        );
        assert_eq!(
            "check\r\n",
            &String::from_utf8(convert_next_line(&mut reader, true).unwrap().unwrap()).unwrap()
        );
        assert_eq!(
            "one\r\n",
            &String::from_utf8(convert_next_line(&mut reader, true).unwrap().unwrap()).unwrap()
        );
        assert_eq!(
            "two\r\n",
            &String::from_utf8(convert_next_line(&mut reader, true).unwrap().unwrap()).unwrap()
        );
        assert_eq!(None, convert_next_line(&mut reader, true).unwrap());
    }
}

#[test]
fn test_convert_line_to_lf() {
    {
        let mut line = b"\n".to_vec();
        convert_line_to_lf(&mut line);
        assert_eq!("\n", &String::from_utf8(line).unwrap());

        let mut line = b"\r\n".to_vec();
        convert_line_to_lf(&mut line);
        assert_eq!("\n", &String::from_utf8(line).unwrap());
    }

    {
        let mut line = b"check".to_vec();
        convert_line_to_lf(&mut line);
        assert_eq!("check", &String::from_utf8(line).unwrap());

        let mut line = b"check\n".to_vec();
        convert_line_to_lf(&mut line);
        assert_eq!("check\n", &String::from_utf8(line).unwrap());

        let mut line = b"check\r\n".to_vec();
        convert_line_to_lf(&mut line);
        assert_eq!("check\n", &String::from_utf8(line).unwrap());
    }

    {
        let mut line = "\u{00e9}".to_string().into_bytes();
        convert_line_to_lf(&mut line);
        assert_eq!("é", &String::from_utf8(line).unwrap());

        let mut line = "\u{00e9}\n".to_string().into_bytes();
        convert_line_to_lf(&mut line);
        assert_eq!("é\n", &String::from_utf8(line).unwrap());

        let mut line = "\u{00e9}\r\n".to_string().into_bytes();
        convert_line_to_lf(&mut line);
        assert_eq!("é\n", &String::from_utf8(line).unwrap());
    }

    {
        let mut line = "\u{0065}\u{0301}".to_string().into_bytes();
        convert_line_to_lf(&mut line);
        assert_eq!("é", &String::from_utf8(line).unwrap());

        let mut line = "\u{0065}\u{0301}\n".to_string().into_bytes();
        convert_line_to_lf(&mut line);
        assert_eq!("é\n", &String::from_utf8(line).unwrap());

        let mut line = "\u{0065}\u{0301}\r\n".to_string().into_bytes();
        convert_line_to_lf(&mut line);
        assert_eq!("é\n", &String::from_utf8(line).unwrap());
    }
}

#[test]
fn test_convert_line_to_crlf() {
    {
        let mut line = b"\n".to_vec();
        convert_line_to_crlf(&mut line);
        assert_eq!("\r\n", &String::from_utf8(line).unwrap());

        let mut line = b"\r\n".to_vec();
        convert_line_to_crlf(&mut line);
        assert_eq!("\r\n", &String::from_utf8(line).unwrap());
    }

    {
        let mut line = b"check".to_vec();
        convert_line_to_crlf(&mut line);
        assert_eq!("check", &String::from_utf8(line).unwrap());

        let mut line = b"check\n".to_vec();
        convert_line_to_crlf(&mut line);
        assert_eq!("check\r\n", &String::from_utf8(line).unwrap());

        let mut line = b"check\r\n".to_vec();
        convert_line_to_crlf(&mut line);
        assert_eq!("check\r\n", &String::from_utf8(line).unwrap());
    }

    {
        let mut line = "\u{00e9}".to_string().into_bytes();
        convert_line_to_crlf(&mut line);
        assert_eq!("é", &String::from_utf8(line).unwrap());

        let mut line = "\u{00e9}\n".to_string().into_bytes();
        convert_line_to_crlf(&mut line);
        assert_eq!("é\r\n", &String::from_utf8(line).unwrap());

        let mut line = "\u{00e9}\r\n".to_string().into_bytes();
        convert_line_to_crlf(&mut line);
        assert_eq!("é\r\n", &String::from_utf8(line).unwrap());
    }

    {
        let mut line = "\u{0065}\u{0301}".to_string().into_bytes();
        convert_line_to_crlf(&mut line);
        assert_eq!("é", &String::from_utf8(line).unwrap());

        let mut line = "\u{0065}\u{0301}\n".to_string().into_bytes();
        convert_line_to_crlf(&mut line);
        assert_eq!("é\r\n", &String::from_utf8(line).unwrap());

        let mut line = "\u{0065}\u{0301}\r\n".to_string().into_bytes();
        convert_line_to_crlf(&mut line);
        assert_eq!("é\r\n", &String::from_utf8(line).unwrap());
    }
}
